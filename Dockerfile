FROM asia.gcr.io/fhs-product-320304/base-image-java:0.1.0

ARG service_name

ENV service_name ${service_name}

ADD target/${service_name}-*.jar /opt/app/${service_name}.jar

EXPOSE 8083

ENTRYPOINT  ["sh","-c","java -jar /opt/app/${service_name}.jar"]

