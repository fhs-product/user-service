package com.fhs.platform.common.errors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fhs.platform.common.constants.CommonMessages;
import com.fhs.platform.common.constants.ConstraintConstants;
import com.fhs.platform.common.errors.exceptions.BusinessException;
import com.fhs.platform.common.errors.exceptions.ConstraintException;
import com.fhs.platform.common.errors.exceptions.EmailAlreadyInUse;
import com.fhs.platform.common.errors.exceptions.ErrorDetail;
import com.fhs.platform.common.errors.exceptions.ErrorMessage;
import com.fhs.platform.common.errors.exceptions.IdentifierAlreadyInUse;
import com.fhs.platform.common.errors.exceptions.ResourceNotFoundException;
import com.fhs.platform.common.utils.MessageUtils;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final List<Class<?>> DATE_CLASSES =
            Arrays.asList(LocalDateTime.class, ZonedDateTime.class, OffsetDateTime.class, LocalDate.class);

    private final MessageUtils messageUtil;

    @Value("${microservice.error.prefix}")
    private String prefix;

    public GlobalExceptionHandler(MessageUtils messageUtil) {
        this.messageUtil = messageUtil;
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(HttpMediaTypeNotSupportedException ex) {

        ErrorMessage errorMessage = new ErrorMessage();

        errorMessage.addErrorDetail(
                new ErrorDetail(prefix,
                        CommonMessages.HTTP_UNSUPPORT_MEDIA_TYPE,
                        messageUtil.getMessage(CommonMessages.HTTP_UNSUPPORT_MEDIA_TYPE)));

        return new ResponseEntity<>(errorMessage, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(HttpMessageNotReadableException ex) {

        Throwable exception = ex.getMostSpecificCause();
        if (exception instanceof InvalidFormatException) {
            return handleException((InvalidFormatException) exception);
        } else if (exception instanceof JsonParseException) {
            return handleException((JsonParseException) exception);
        } else if (exception instanceof JsonMappingException) {
            return handleException((JsonMappingException) exception);
        } else if (exception instanceof DateTimeException) {
            return handleException((DateTimeException) exception);
        } else if (exception instanceof IllegalArgumentException) {
            return handleException((IllegalArgumentException) exception);
        }

        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(new ErrorDetail(prefix, CommonMessages.HTTP_BAD_REQUEST, ex.getMessage()));

        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(InvalidFormatException ex) {
        ErrorDetail errorDetail =
                new ErrorDetail(prefix, CommonMessages.HTTP_BAD_REQUEST, ex.getOriginalMessage());
        Class<?> targetClass = ex.getTargetType();
        Object[] enumConstants = targetClass.getEnumConstants();
        int refSize = ex.getPath().size();

        if (ArrayUtils.isNotEmpty(enumConstants)) {
            errorDetail = populateFieldErrorForEnum(ex);
        } else if (refSize > 0) {
            String message;
            String errorCode;
            String fieldName;

            fieldName = buildFieldPath(ex.getPath());
            if (Number.class.isAssignableFrom(targetClass)) {
                errorCode = CommonMessages.NUMBER;
                message = messageUtil.getMessage(errorCode, fieldName);
            } else if (isDateClass(targetClass)) {
                errorCode = CommonMessages.DATETIME;
                message = messageUtil.getMessage(errorCode, fieldName);
            } else {
                errorCode = CommonMessages.INVALID_FORMAT;
                message = messageUtil.getMessage(errorCode, fieldName, targetClass.getSimpleName());
            }
            errorDetail.setCode(prefix.concat(errorCode));
            errorDetail.setDetails(message);
        }

        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(errorDetail);

        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(JsonParseException ex) {
        ErrorDetail errorDetail =
                new ErrorDetail(prefix, CommonMessages.HTTP_BAD_REQUEST, ex.getOriginalMessage());
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(errorDetail);

        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(EmailAlreadyInUse ex) {
        ErrorDetail errorDetail =
                new ErrorDetail(prefix, CommonMessages.HTTP_BAD_REQUEST, "email " + ex.getEmail() + " already in use");
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(errorDetail);
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(IdentifierAlreadyInUse ex) {
        ErrorDetail errorDetail =
                new ErrorDetail(prefix, CommonMessages.HTTP_BAD_REQUEST,
                        ex.getIdentifierType().getDescription() + " " + ex.getValue() + " already in use");
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(errorDetail);
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(MaxUploadSizeExceededException ex) {

        ErrorDetail errorDetail =
                new ErrorDetail(prefix, CommonMessages.HTTP_BAD_REQUEST, ex.getMessage());
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(errorDetail);
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(Throwable ex) {

        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(
                new ErrorDetail(prefix, CommonMessages.HTTP_INTERNAL_SERVER_ERROR,
                        messageUtil.getMessage(CommonMessages.HTTP_INTERNAL_SERVER_ERROR)));

        return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(IllegalArgumentException ex) {

        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(
                new ErrorDetail(prefix, CommonMessages.HTTP_BAD_REQUEST,
                        ex.getLocalizedMessage()));

        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(DateTimeException ex) {
        ErrorDetail errorDetail = new ErrorDetail(prefix, CommonMessages.DATETIME, ex.getMessage());
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(errorDetail);
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(JsonMappingException ex) {
        String fieldPath = buildFieldPath(ex.getPath());
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(
                new ErrorDetail(prefix,
                        CommonMessages.BAD_SYNTAX,
                        messageUtil.getMessage(CommonMessages.BAD_SYNTAX, fieldPath)));
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    private ErrorDetail populateFieldErrorForEnum(InvalidFormatException ex) {
        Class<?> targetClass = ex.getTargetType();
        Object[] enumConstants = targetClass.getEnumConstants();

        List<Reference> path = ex.getPath();
        Reference ref = path.get(path.size() - 1);

        ErrorDetail errorDetail = new ErrorDetail();
        errorDetail.setCode(CommonMessages.VALIDATION_INVALID_NOT_IN_RANGE);
        errorDetail.setDetails(
                messageUtil.getMessage(
                        CommonMessages.VALIDATION_INVALID_NOT_IN_RANGE,
                        ref.getFieldName(),
                        Arrays.asList(enumConstants)));

        return errorDetail;
    }

    private boolean isDateClass(Class<?> targetClass) {
        return DATE_CLASSES.stream().anyMatch(targetClass::isAssignableFrom);
    }

    private String buildFieldPath(List<Reference> references) {
        StringBuilder path = new StringBuilder();
        for (int i = 0; i < references.size(); i++) {
            Reference reference = references.get(i);
            if (reference.getIndex() >= 0) {
                path.append("[").append(reference.getIndex()).append("]");
            } else {
                if (i > 0) {
                    path.append(".");
                }
                path.append(Objects.toString(reference.getFieldName(), ""));
            }
        }
        return path.toString();
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(MethodArgumentNotValidException ex) {
        return processBindingResult(ex.getBindingResult());
    }

    private ResponseEntity<ErrorMessage> processBindingResult(BindingResult result) {
        ErrorMessage errorMessage = new ErrorMessage();
        List<ErrorDetail> errors = new ArrayList<>();
        if (result.hasGlobalErrors()) {
            List<ObjectError> objectErrors = result.getGlobalErrors();
            errors.addAll(processObjectErrors(objectErrors).getIssues());
        }
        List<FieldError> fieldErrors = result.getFieldErrors();
        errors.addAll(processFieldErrors(fieldErrors).getIssues());
        errorMessage.setIssues(errors);

        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    private ErrorMessage processObjectErrors(List<ObjectError> objectErrors) {
        ErrorMessage dto = new ErrorMessage();
        for (ObjectError objectError : objectErrors) {
            dto.addErrorDetail(this.populateObjectError(objectError));
        }
        return dto;
    }

    private ErrorMessage processFieldErrors(List<FieldError> fieldErrors) {
        ErrorMessage dto = new ErrorMessage();

        for (FieldError fieldError : fieldErrors) {
            dto.addErrorDetail(this.populateFieldError(fieldError));
        }

        return dto;
    }

    private ErrorDetail populateObjectError(ObjectError objectError) {
        String[] objectErrorCodes = Optional.ofNullable(objectError.getCodes()).orElse(new String[0]);
        Object[] args = Optional.ofNullable(objectError.getArguments()).orElse(new Object[0]);

        return getErrorDetail(objectError.getObjectName(), null, objectErrorCodes, args);
    }

    private ErrorDetail populateFieldError(FieldError fieldError) {

        String fieldName = fieldError.getField();
        var value = fieldError.getRejectedValue();
        String[] fieldErrorCodes = Optional.ofNullable(fieldError.getCodes()).orElse(new String[0]);
        Object[] args = Optional.ofNullable(fieldError.getArguments()).orElse(new Object[0]);

        return getErrorDetail(fieldName, value, fieldErrorCodes, args);
    }

    private ErrorDetail getErrorDetail(String fieldName, Object rejectedValue, String[] fieldErrorCodes, Object[] args) {
        String errorCode;

        if (ArrayUtils.isNotEmpty(fieldErrorCodes)) {
            errorCode = fieldErrorCodes[fieldErrorCodes.length - 1];
        } else {
            errorCode = ConstraintConstants.DEFAULT.errorCode();
        }

        ConstraintConstants constraint = ConstraintConstants.valueOfErrorCode(errorCode);

        String code = constraint.messageCode();
        String message = constraint.message(messageUtil, rejectedValue, fieldName, args);

        return new ErrorDetail(prefix, code, message);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(ConstraintException ex) {
        return processBindingResult(ex.getBindingResult());
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(ResourceNotFoundException ex) {

        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.addErrorDetail(
                new ErrorDetail(prefix, CommonMessages.ENTITY_NOTFOUND,
                        messageUtil.getMessage(CommonMessages.ENTITY_NOTFOUND, ex.getResourceId())));

        return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<ErrorMessage> handleException(BusinessException ex) {
        ErrorMessage errorMessage = ex.getErrorMessage(prefix, messageUtil);
        return new ResponseEntity<>(errorMessage, errorMessage.getStatus());
    }


}


