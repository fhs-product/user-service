
package com.fhs.platform.common.errors.exceptions;

public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -6523881796133573424L;

    private final String resourceId;

    public ResourceNotFoundException(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceId() {
        return resourceId;
    }

}
