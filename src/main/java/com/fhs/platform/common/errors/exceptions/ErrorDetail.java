
package com.fhs.platform.common.errors.exceptions;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fhs.platform.common.errors.Severity;
import java.io.Serializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ErrorDetail implements Serializable {

    private static final long serialVersionUID = -7046567216197159057L;

    private String severity = Severity.ERROR.toString();
    private String code;
    private String details;

    public ErrorDetail(String prefix, HttpStatus code, String details) {
        super();
        this.code = prefix.concat(code.name());
        this.details = details;
    }

    public ErrorDetail(String prefix, String code, String details) {
        super();
        this.code = prefix.concat(code);
        this.details = details;
    }

    public ErrorDetail(String prefix, String code, String severity, String details) {
        super();
        this.code = prefix.concat(code);
        if (!StringUtils.isBlank(severity)) {
            this.severity = severity;
        }
        this.details = details;
    }

    public ErrorDetail(String code, String message) {
        super();
        this.code = code;
        this.details = message;
    }

}
