package com.fhs.platform.common.errors.exceptions;

import com.fhs.platform.service.user.constants.identifiertype.IdentifierType;

public class IdentifierAlreadyInUse extends RuntimeException {

    private static final long serialVersionUID = -6523881796133573424L;
    private final IdentifierType identifierType;
    private final String value;

    public IdentifierAlreadyInUse(IdentifierType identifierType, String value) {
        this.identifierType = identifierType;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public IdentifierType getIdentifierType() {
        return identifierType;
    }

}
