package com.fhs.platform.common.errors.exceptions;

public class EmailAlreadyInUse extends RuntimeException {

    private static final long serialVersionUID = -6523881796133573424L;

    private final String email;

    public EmailAlreadyInUse(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

}
