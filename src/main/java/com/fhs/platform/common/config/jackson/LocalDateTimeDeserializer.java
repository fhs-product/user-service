
package com.fhs.platform.common.config.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fhs.platform.common.constants.DateTimeFormatConstants;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        String text = parser.getText();
        if (text == null) {
            return null;
        }

        String key = parser.getText().trim();
        try {
            return LocalDateTime.parse(key, DateTimeFormatConstants.LOCAL_DATE_TIME_FORMATTER);
        } catch (DateTimeException ex) {
            throw InvalidFormatException.from(parser, ex.getMessage(), text, ZonedDateTime.class);
        }
    }

}
