
package com.fhs.platform.common.config.auth;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BearerContext implements Serializable {

    private static final long serialVersionUID = 1L;
    private String roleName;
    private String userId;
    private String bearerToken;

}
