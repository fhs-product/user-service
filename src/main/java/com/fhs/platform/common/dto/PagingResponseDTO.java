package com.fhs.platform.common.dto;

import java.util.Collection;
import java.util.Collections;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

@Setter
@Getter
public class PagingResponseDTO<T> {
    private long totalCount;
    private Collection<T> data;
    private int pageSize;
    private int pageNumber;

    public PagingResponseDTO(Page<T> page) {
        if (page == null) {
            page = new PageImpl<>(Collections.emptyList());
        }
        this.totalCount = page.getTotalElements();
        this.data = page.getContent();
        this.pageNumber = page.getNumber()+1;
        this.pageSize = page.getSize();
    }

}
