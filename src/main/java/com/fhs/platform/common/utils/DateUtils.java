
package com.fhs.platform.common.utils;

import com.fhs.platform.common.constants.DateTimeFormatConstants;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import org.springframework.stereotype.Component;

@Component
public class DateUtils {

    private DateUtils() {
        // Private constructor
    }

    public static OffsetDateTime toOffsetDateTime(ZonedDateTime zonedDateTime) {
        return zonedDateTime != null ? zonedDateTime.toOffsetDateTime() : null;
    }

    public static ZonedDateTime toZonedDateTime(OffsetDateTime offsetDateTime) {
        return offsetDateTime != null
                ? offsetDateTime.atZoneSameInstant(DateTimeFormatConstants.UTC_ZONE_ID)
                : null;
    }

    public static Timestamp toTimestamp(OffsetDateTime offsetDateTime) {
        return offsetDateTime != null
                ? Timestamp.valueOf(offsetDateTime.atZoneSameInstant(ZoneOffset.UTC).toLocalDateTime())
                : null;
    }

    public static OffsetDateTime toOffsetDateTime(Timestamp timestamp) {
        return timestamp != null ? OffsetDateTime.of(timestamp.toLocalDateTime(), ZoneOffset.UTC) : null;
    }

}
