
package com.fhs.platform.common.logging.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by vietnguyen on 9/15/20
 */

public class JSONUtil {
    public static final String DATE_TIME_FORMAT_TIME_ZONE = "yyyy-MM-dd'T'HH:mm:ssZ";
    private static final String MASKING_VALUE = "****";
    private static final String JSON_PROCESSING_LOG_MSG = "JSON Processing Exception";

    private JSONUtil() {

    }

    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * @param object       object that needs converting to JSON form
     * @param maskableKeys field names indicating that it needs hiding
     * @return the JSON string of the object. Return an empty string when having exception
     */
    public static String parseObjectToString(Object object, Set<String> maskableKeys) {
        try {
            registerTimeModule(objectMapper);
            return doMask(objectMapper.writeValueAsString(object), maskableKeys);
        } catch (IOException e) {
            return JSON_PROCESSING_LOG_MSG;
        }
    }

    public static void registerTimeModule(ObjectMapper mapper) {
        JavaTimeModule module = new JavaTimeModule();
        mapper.registerModule(module);
        mapper.configure(SerializationFeature.WRITE_DATE_KEYS_AS_TIMESTAMPS, true);
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT_TIME_ZONE);
        mapper.setDateFormat(dateFormat);
    }

    private static boolean isValidSet(Set<String> set) {
        return set != null && !set.isEmpty();
    }

    private static boolean isKnownPrimitiveWrapperModel(Object obj) {
        return obj == null || obj instanceof IntNode || obj instanceof LongNode
                || obj instanceof DoubleNode || obj instanceof TextNode;
    }


    private static JsonNode maskingForObjectNode(Set<String> maskableKeys, JsonNode input) {
        if (!isValidSet(maskableKeys) || input == null) {
            return input;
        }

        Iterator<String> fieldNames = input.fieldNames();
        while (fieldNames.hasNext()) {
            String fieldName = fieldNames.next();
            JsonNode fieldValue = input.get(fieldName);
            if (fieldValue instanceof ArrayNode) {
                maskingForArrayNode(maskableKeys, fieldName, (ArrayNode) fieldValue);
            } else if (fieldValue instanceof ObjectNode) {
                maskingForObjectNode(maskableKeys, fieldValue);
            } else if (fieldName != null && maskableKeys.contains(fieldName.toLowerCase())) {
                ((ObjectNode) input).put(fieldName, MASKING_VALUE);
            }
        }
        return input;
    }

    private static ArrayNode maskingForArrayNode(Set<String> maskableKeys, String key,
                                                 ArrayNode jsonArr) {
        ArrayNode toRet = jsonArr;
        for (int idx = 0; idx < toRet.size(); idx++) {
            Object obj = toRet.get(idx);
            if (isKnownPrimitiveWrapperModel(obj)) {
                if (key != null && maskableKeys.contains(key.toLowerCase())) {
                    toRet.set(idx, TextNode.valueOf(MASKING_VALUE));
                }
            } else {
                JsonNode jsonObjFromArray = toRet.get(idx);
                JsonNode maskedJsonObj = maskingForObjectNode(maskableKeys, jsonObjFromArray);
                toRet.set(idx, maskedJsonObj);
            }
        }
        return toRet;
    }

    private static String doMask(String input, Set<String> maskableKeys) throws IOException {
        String maskedData = input;

        if (maskedData != null && !maskedData.trim().isEmpty()) {
            JsonNode jsonNode = objectMapper.readTree(input);
            if (jsonNode instanceof ObjectNode) {
                ObjectNode maskedOutput = (ObjectNode) maskingForObjectNode(maskableKeys, jsonNode);
                maskedData = objectMapper.writeValueAsString(maskedOutput);
            } else if (jsonNode instanceof ArrayNode) {
                ArrayNode maskedOutput = maskingForArrayNode(maskableKeys, null, (ArrayNode) jsonNode);
                maskedData = objectMapper.writeValueAsString(maskedOutput);
            }

        }
        return maskedData;
    }
}
