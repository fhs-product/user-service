package com.fhs.platform.common.constants;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateTimeFormatConstants {

    public static final String ZONE_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";

    public static final String LOCAL_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String TIME_FORMAT = "HH:mm";

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String DEFAULT_TIME_ZONE = "Asia/Bangkok";

    public static final DateTimeFormatter ZONE_DATE_TIME_FORMATTER =
            DateTimeFormatter.ofPattern(ZONE_DATE_TIME_FORMAT);

    public static final DateTimeFormatter LOCAL_DATE_TIME_FORMATTER =
            DateTimeFormatter.ofPattern(LOCAL_DATE_TIME_FORMAT);

    public static final DateTimeFormatter DATE_FORMAT_FORMATTER =
            DateTimeFormatter.ofPattern(DATE_FORMAT);

    public static final DateTimeFormatter TIME_FORMAT_FORMATTER =
            DateTimeFormatter.ofPattern(TIME_FORMAT);

    public static final DateTimeFormatter DATE_TIME_FORMAT_FORMATTER =
            DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);

    public static final ZoneId UTC_ZONE_ID = ZoneId.of(DEFAULT_TIME_ZONE);

    private DateTimeFormatConstants() {
        // Private constructor
    }

}
