
package com.fhs.platform.common.constants;

public final class CommonMessages {

    /**
     * Mandatory field validation message. Format: {0} is required.
     */
    public static final String REQUIRED = "PARAM_REQUIRED";

    /**
     * Min length validation message. Format: {0} is required to be at least {1} characters.
     */
    public static final String MINLENGTH = "MUST_NOT_BE_SHORTER_THAN";

    /**
     * Max length validation message. Format: {0} cannot be longer than {1} characters.
     */
    public static final String MAXLENGTH = "MUST_NOT_BE_LONGER_THAN";

    /**
     * Min length & max length validation message. Format: {0} size must be between {1} and {2}.
     */
    public static final String SIZE_BETWEEN = "MUST_BE_BETWEEN";

    /**
     * Number validation. Format: {0} must be less than or equal to {1}.
     */
    public static final String NUMBER_MAX = "MUST_BE_LESS_THAN_OR_EQUAL";

    /**
     * Number validation. Format: {0} must be greater than or equal to {1}.
     */
    public static final String NUMBER_MIN = "MUST_BE_GREATER_THAN_OR_EQUAL";

    /**
     * Pattern validation message. Format: {0} should follow pattern for {1}.
     */
    public static final String PATTERN = "MUST_MATCH_PATTERN";

    /**
     * Number field only validation message. Format: {0} should be a number.
     */
    public static final String NUMBER = "MUST_BE_NUMBER";

    /**
     * Field must contains number only validation message. Format: {0} can only contain numbers.
     */
    public static final String CONTAINS_NUMBER = "MUST_CONTAIN_NUMBERS";

    /**
     * Datetime validation message. Format: {0} should be a datetime.
     */
    public static final String DATETIME = "DATE_FORMAT";

    /**
     * Field is invalid validation message. Format: {0} is invalid.
     */
    public static final String INVALID = "PARAM_INVALID";

    /**
     * Datetime validation message. Format: {0} must be in the past.
     */
    public static final String PAST = "MUST_BE_IN_PAST";

    /**
     * Datetime validation message. Format: {0} must be in the future.
     */
    public static final String FUTURE = "MUST_BE_IN_FUTURE";

    /**
     * Value should be a well-formed email address
     */
    public static final String EMAIL = "EMAIL_FORMAT";

    /**
     * Value should be one of pre-defined ranges
     */
    public static final String VALIDATION_INVALID_NOT_IN_RANGE = "MUST_BE_ONE_OF";

    public static final String MUST_BE_EQUAL = "MUST_BE_EQUAL";

    public static final String DOES_NOT_MATCH = "MUST_MATCH_WITH";

    /**
     * No mobile phone number found for user
     */
    public static final String NO_MOBILE_PHONE_NUMBER_FOUND = "NO_MOBILE_PHONE_NUMBER_FOUND";

    /**
     * Value : Json Source for a resource should start with an object
     */
    public static final String JSON_OBJECT = "JSON_OBJECT";

    /**
     * Value : Bad Syntax in value
     */
    public static final String BAD_SYNTAX = "BAD_SYNTAX";

    /**
     * Message for HTTP_RESPONSE 201 - Created
     */
    public static final String HTTP_CREATED = "HTTP_STATUS_CODE_201";

    /**
     * Message for HTTP_RESPONSE 204 - No Content
     */
    public static final String HTTP_NO_CONTENT = "HTTP_STATUS_CODE_204";

    /**
     * Error message for HTTP_RESPONSE 400 - Bad request
     */
    public static final String HTTP_BAD_REQUEST = "BAD_REQUEST";

    /**
     * Error message for HTTP_RESPONSE 401 - Unauthorized
     */
    public static final String HTTP_UNAUTHORIZED = "AUTH_REQUIRED";

    /**
     * Error message for HTTP_RESPONSE 403 - Access denied
     */
    public static final String HTTP_ACCESS_DENIED = "PERMISSION_DENIED";

    /**
     * Error message for HTTP_RESPONSE 404 - Page not found
     */
    public static final String HTTP_PAGE_NOT_FOUND = "NO_EXIST";

    /**
     * Error message for HTTP_RESPONSE 408 - Request Timeout
     */
    public static final String HTTP_REQUEST_TIMEOUT = "REQ_TIMEOUT";

    /**
     * Error message for HTTP_RESPONSE 415 - Unsupported Media Type
     */
    public static final String HTTP_UNSUPPORT_MEDIA_TYPE = "UNSUPPORTED_MEDIA_TYPE";

    /**
     * Error message for HTTP_RESPONSE 409 - Resource State Conflict
     */
    public static final String HTTP_RESOURCE_STATE_CONFLICT = "RESOURCE_STATE_CONFLICT";

    /**
     * Error message for HTTP_RESPONSE 423 - Resource Locked
     */
    public static final String HTTP_RESOURCE_LOCKED = "RESOURCE_STATE_LOCKED";

    /**
     * Error message for HTTP_RESPONSE 500 - Internal server error
     */
    public static final String HTTP_INTERNAL_SERVER_ERROR = "INTERNAL_ERROR";

    /**
     * Error message for HTTP_RESPONSE 501 - Not implemented
     */
    public static final String HTTP_NOT_IMPLEMENTED = "NOT_IMPLEMENTED";

    /**
     * Error message for concurrency modify
     */
    public static final String CONCURRENCY = "CONFLICT_CHANGE";

    /**
     * Error message for Entity not found
     */
    public static final String ENTITY_NOTFOUND = "NO_EXIST";

    /**
     * Error message for Entity already exists
     */
    public static final String ENTITY_EXISTS = "ENTITY_ALREADY_IN_USE";

    /**
     * Error message when request has an invalid format
     */
    public static final String INVALID_FORMAT = "BAD_FORMAT";

    /**
     * Error message when a date period is invalid (start date after end date).
     */
    public static final String INVALID_PERIOD = "INVALID_PERIOD";

    /**
     * Error message when request data is not in a pre-defined values
     */

    public static final String ENTITY_WITH_DETAIL_NOTFOUND = "ENTITY_WITH_DETAIL_NOT_FOUND";

    public static final String ENTITY_NOT_FOUND_OR_MISS_MATCH = "ENTITY_NOT_FOUND_OR_MISS_MATCH";

    public static final String CALL_OTHER_SERVICE = "SERVICE_CALL";

    public static final String ENTITY_NOT_FOUND_MULTIPLE_PARAM_OR_MISS_MATCH = "ENTITY_MULT_PARAM_MISS_MATCH";

    public static final String ABORTED_TRANSACTION = "ABORTED_TRANSACTION";

    public static final String UNKNOWN_LOG_LEVEL = "UNKNOWN_LOG_LEVEL";

    public static final String UNABLE_TO_RETRIEVE_LOG_LEVEL = "UNABLE_TO_RETRIEVE_LOG_LEVEL";

    public static final String SPANNER_RETRY_FAILURE = "RETRY_FAILURE";

    public static final String RESTRICTION_MODE = "RESTRICTION_MODE";

    public static final String NOT_ENABLED = "NOT_ENABLED";

    public static final String INVALID_SORT_BY = "SORT_UNKNOWN";

    public static final String EMAILS = "EMAIL_LIST";

    public static final String EMAILS_MAXLENGTH = "EMAIL_LIST_MAXLENGTH";

    public static final String FIELDS_MUTUALLY_EXCLUSIVE = "FIELDS_MUTUALLY_EXCLUSIVE";

    /**
     * Error message when upload file
     */
    public static final String INVALID_CSV_FILE_SIZE = "INVALID_FILE_SIZE";

    public static final String INVALID_CSV_CONTENT_FORMAT = "INVALID_CONTENT_FORMAT";

    public static final String CHOOSE_EMPTY_CSV = "EMPTY_FILE";

    public static final String INVALID_CSV_FILE_FORMAT = "INVALID_FILE_FORMAT";

    /**
     * Error message for version mismatches
     */
    public static final String VERSION_UPDATE_REQUIRED = "VERSION_AWARE";

    public static final String VERSION_UPDATE_CONFLICT = "VERSION_AWARE_CONFLICT";

    private CommonMessages() {
        // Private constructor
    }

}
