package com.fhs.platform.service.user.controller;

import com.fhs.platform.common.config.auth.BearerContextHolder;
import com.fhs.platform.common.dto.DeleteResponse;
import com.fhs.platform.common.dto.PagingResponseDTO;
import com.fhs.platform.service.user.constants.responsestatus.ResponseStatus;
import com.fhs.platform.service.user.controller.request.create.AddressCreateDto;
import com.fhs.platform.service.user.controller.request.create.ContactCreateDto;
import com.fhs.platform.service.user.controller.request.create.IdentifierCreateDto;
import com.fhs.platform.service.user.controller.request.create.PatientCreateDto;
import com.fhs.platform.service.user.controller.request.update.AddressUpdateDto;
import com.fhs.platform.service.user.controller.request.update.ContactUpdateDto;
import com.fhs.platform.service.user.controller.request.update.IdentifierUpdateDto;
import com.fhs.platform.service.user.controller.request.update.PatientUpdateDto;
import com.fhs.platform.service.user.controller.response.AddressResponseDto;
import com.fhs.platform.service.user.controller.response.ContactResponseDto;
import com.fhs.platform.service.user.controller.response.AttachmentResponseDto;
import com.fhs.platform.service.user.controller.response.IdentifierResponseDto;
import com.fhs.platform.service.user.controller.response.PatientResponseDto;
import com.fhs.platform.service.user.domain.PatientFilter;
import com.fhs.platform.service.user.exceptions.EntityAlreadyExists;
import com.fhs.platform.service.user.response.ApiResponse;
import com.fhs.platform.service.user.service.PatientService;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/patients")
    public ResponseEntity<?> getPatients(
            @PageableDefault(sort = {"familyName"}, size = 10) Pageable pageable,
            PatientFilter filter) {
        if (BearerContextHolder.getContext().getRoleName()
                .equalsIgnoreCase("patient")) {
            filter.setId(BearerContextHolder.getContext().getUserId());
        }
        PagingResponseDTO<?> patientDtos = patientService.getPatients(pageable, filter);
        if (patientDtos.getData().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(patientDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/patients/{patientId}")
    public ResponseEntity<?> getPatientById(@PathVariable("patientId") UUID patientId) {

        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }

        PatientResponseDto patientDto = patientService.getPatientById(patientId);
        return new ResponseEntity<>(patientDto, HttpStatus.OK);
    }

    @GetMapping(value = "/patients/{patientId}/addresses/{addressId}")
    public ResponseEntity<?> getPatientAddressById(@PathVariable("patientId") UUID patientId, @PathVariable("addressId") UUID addressId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        AddressResponseDto addressDto = patientService.getPatientAddressById(patientId, addressId);
        return new ResponseEntity<>(addressDto, HttpStatus.OK);

    }

    @GetMapping(value = "/patients/{patientId}/identifiers/{identifierId}")
    public ResponseEntity<?> getPatientIdentifierById(@PathVariable("patientId") UUID patientId, @PathVariable("identifierId") UUID identifierId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        IdentifierResponseDto identifierDto = patientService.getPatientIdentifierById(patientId, identifierId);
        return new ResponseEntity<>(identifierDto, HttpStatus.OK);

    }

    @GetMapping(value = "/patients/{patientId}/contacts/{contactId}")
    public ResponseEntity<?> getPatientContactById(@PathVariable UUID patientId, @PathVariable UUID contactId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        ContactResponseDto contactDto = patientService.getPatientContactById(patientId, contactId);
        return new ResponseEntity<>(contactDto, HttpStatus.OK);

    }

    @GetMapping(value = "/patients/{patientId}/addresses")
    public ResponseEntity<?> getPatientAddresses(@PathVariable UUID patientId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        List<AddressResponseDto> addressDtos = patientService.getPatientAddresses(patientId);
        if (addressDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(addressDtos, HttpStatus.OK);

    }

    @GetMapping(value = "/patients/{patientId}/identifiers")
    public ResponseEntity<?> getPatientIdentifiers(@PathVariable UUID patientId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        List<IdentifierResponseDto> identifierDtos = patientService.getPatientIdentifiers(patientId);
        if (identifierDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(identifierDtos, HttpStatus.OK);


    }

    @PostMapping(value = "/patients")
    public ResponseEntity<?> createPatient(@Valid @RequestBody PatientCreateDto patientCreateDto) {

        try {
            PatientResponseDto patientResponseDto = patientService.createPatient(patientCreateDto);
            return new ResponseEntity<>(patientResponseDto, HttpStatus.CREATED);
        } catch (EntityAlreadyExists e) {
            return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/patients/{patientId}/contacts")
    public ResponseEntity<?> getPatientContacts(@PathVariable UUID patientId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        List<ContactResponseDto> contactDtos = patientService.getPatientContacts(patientId);
        if (contactDtos.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(contactDtos, HttpStatus.OK);


    }

    @PostMapping(value = "/patients/{patientId}/addresses")
    public ResponseEntity<?> createPatientAddress(@PathVariable UUID patientId, @RequestBody @Valid AddressCreateDto addressCreateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        AddressResponseDto addressResponseDto = patientService.createPatientAddress(patientId, addressCreateDto);
        return new ResponseEntity<>(addressResponseDto, HttpStatus.CREATED);

    }


    @PostMapping(value = "/patients/{patientId}/identifiers")
    public ResponseEntity<?> createPatientIdentifier(@PathVariable UUID patientId, @RequestBody @Valid IdentifierCreateDto identifierCreateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        try {
            IdentifierResponseDto identifierResponseDto = patientService.createPatientIdentifier(patientId, identifierCreateDto);
            return new ResponseEntity<>(identifierResponseDto, HttpStatus.CREATED);
        } catch (EntityAlreadyExists e) {
            return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, e.getMessage()), HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "/patients/{patientId}/contacts")
    public ResponseEntity<?> createPatientContact(@PathVariable UUID patientId, @RequestBody @Valid ContactCreateDto contactCreateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        ContactResponseDto contactResponseDto = patientService.createPatientContact(patientId, contactCreateDto);
        return new ResponseEntity<>(contactResponseDto, HttpStatus.CREATED);


    }

    @PutMapping(value = "/patients/{patientId}")
    public ResponseEntity<?> updatePatient(@PathVariable("patientId") UUID patientId, @RequestBody PatientUpdateDto patientUpdateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        PatientResponseDto patientResponseDto = patientService.updatePatientById(patientId, patientUpdateDto);
        return new ResponseEntity<>(patientResponseDto, HttpStatus.OK);
    }

    @PutMapping(value = "/patients/{patientId}/addresses/{addressId}")
    public ResponseEntity<?> updatePatientAddress(@PathVariable("patientId") UUID patientId, @PathVariable("addressId") UUID addressId,
            @RequestBody AddressUpdateDto addressUpdateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        AddressResponseDto addressResponseDto = patientService.updatePatientAddress(patientId, addressId, addressUpdateDto);
        return new ResponseEntity<>(addressResponseDto, HttpStatus.OK);
    }

    @PutMapping(value = "/patients/{patientId}/identifiers/{identifierId}")
    public ResponseEntity<?> updatePatientIdentifier(@PathVariable("patientId") UUID patientId, @PathVariable("identifierId") UUID identifierId,
            @RequestBody IdentifierUpdateDto identifierUpdateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        try {
            IdentifierResponseDto identifierResponseDto = patientService.updatePatientIdentifier(patientId, identifierId, identifierUpdateDto);
            return new ResponseEntity<>(identifierResponseDto, HttpStatus.OK);
        } catch (EntityAlreadyExists e) {
            return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/patients/{patientId}/contacts/{contactId}")
    public ResponseEntity<?> updatePatientContact(@PathVariable("patientId") UUID patientId, @PathVariable("contactId") UUID contactId,
            @RequestBody ContactUpdateDto contactUpdateDto) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }

        ContactResponseDto contactResponseDto = patientService.updatePatientContact(patientId, contactId, contactUpdateDto);
        return new ResponseEntity<>(contactResponseDto, HttpStatus.OK);

    }

    @DeleteMapping(value = "/patients/{patientId}")
    public ResponseEntity<?> deletePatient(@PathVariable("patientId") UUID patientId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        DeleteResponse deleteDto = patientService.deletePatientById(patientId);
        return new ResponseEntity<>(deleteDto, HttpStatus.OK);

    }

    @DeleteMapping(value = "/patients/{patientId}/addresses/{addressId}")
    public ResponseEntity<?> deletePatientAddress(@PathVariable("patientId") UUID patientId, @PathVariable("addressId") UUID addressId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }

        DeleteResponse deleteResponse = patientService.deletePatientAddressById(patientId, addressId);
        return new ResponseEntity<>(deleteResponse, HttpStatus.OK);

    }

    @DeleteMapping(value = "/patients/{patientId}/identifiers/{identifierId}")
    public ResponseEntity<?> deletePatientIdentifier(@PathVariable("patientId") UUID patientId, @PathVariable("identifierId") UUID identifierId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }

        DeleteResponse deleteResponse = patientService.deletePatientIdentifier(patientId, identifierId);
        return new ResponseEntity<>(deleteResponse, HttpStatus.OK);

    }

    @DeleteMapping(value = "/patients/{patientId}/contacts/{contactId}")
    public ResponseEntity<?> deletePatientContact(@PathVariable("patientId") UUID patientId, @PathVariable("contactId") UUID contactId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        DeleteResponse deleteResponse = patientService.deletePatientContact(patientId, contactId);
        return new ResponseEntity<>(deleteResponse, HttpStatus.OK);
    }


    @PostMapping("/patients/{patientId}/avatar")
    public ResponseEntity<?> uploadAvatar(
            @PathVariable("patientId") UUID patientId,
            @RequestParam("avatar") MultipartFile avatar) throws IOException {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }


        if(!Arrays.asList(ContentType.IMAGE_JPEG.getMimeType(), ContentType.IMAGE_PNG.getMimeType(), ContentType.IMAGE_GIF.getMimeType()).contains(avatar.getContentType())) {
            return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE,"file must be JPEG, PNG or GIF"), HttpStatus.BAD_REQUEST);
        }
        AttachmentResponseDto attachmentResponseDto = patientService.uploadAvatarToPatient(avatar, patientId);
        return new ResponseEntity<>(attachmentResponseDto, HttpStatus.OK);
    }


    @GetMapping("/patients/{patientId}/avatar/{filename:.+}")
    public ResponseEntity<?> getAvatar(@PathVariable("filename") String filename, @PathVariable("patientId") UUID patientId) {
        if (!(BearerContextHolder.getContext().getRoleName().equalsIgnoreCase("admin"))) {
            if (BearerContextHolder.getContext().getRoleName()
                    .equalsIgnoreCase("patient")) {
                patientId = UUID.fromString(BearerContextHolder.getContext().getUserId());
            } else {
                return new ResponseEntity<>(new ApiResponse(ResponseStatus.FAILURE, "Unauthorized"), HttpStatus.UNAUTHORIZED);
            }
        }
        Resource file = patientService.loadAvatarFile(filename, patientId);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }




}
