package com.fhs.platform.service.user.controller.request.create;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fhs.platform.common.config.jackson.LocalDateDeserializer;
import com.fhs.platform.common.config.jackson.LocalDateSerializer;
import com.fhs.platform.common.constants.DateTimeFormatConstants;
import java.time.LocalDate;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data

public class PractitionerCreateDto {

    @NotNull
    private String familyName;
    @NotNull
    private String givenName;
    @NotNull
    private String prefixName;
    private String suffixName;
    @NotNull
    private String gender;
    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormatConstants.DATE_FORMAT)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)

    private LocalDate birthDate;
    @NotNull
    private String email;
    @NotNull
    private String experience;
    @NotNull
    private String specialty;
    @Valid
    private Set<QualificationCreateDto> qualifications;
    @Valid
    private Set<AddressCreateDto> addresses;
    @Valid
    private Set<IdentifierCreateDto> identifiers;

}
