package com.fhs.platform.service.user.util.mapper;

import com.fhs.platform.service.user.controller.request.create.QualificationCreateDto;
import com.fhs.platform.service.user.controller.request.update.QualificationUpdateDto;
import com.fhs.platform.service.user.controller.response.QualificationResponseDto;
import com.fhs.platform.service.user.entity.Qualification;
import org.mapstruct.BeanMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = {
}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface QualificationMapper {

    @Mapping(target = "practitioner", ignore = true)
    Qualification qualificationDtoToQualification(QualificationCreateDto qualificationCreateDto);
    @Mapping(target = "startValid", dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX")
    @Mapping(target = "endValid", dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX")
    QualificationResponseDto qualificationToQualificationResponseDto(Qualification qualification);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateQualificationFromDto(QualificationUpdateDto qualificationUpdateDto, @MappingTarget Qualification qualification);

}
