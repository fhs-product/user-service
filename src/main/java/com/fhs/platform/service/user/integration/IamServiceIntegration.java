package com.fhs.platform.service.user.integration;

import com.fhs.platform.common.errors.exceptions.BusinessException;
import com.fhs.platform.service.user.constants.IamIntegrationConstants;
import com.fhs.platform.service.user.entity.Patient;
import com.fhs.platform.service.user.entity.Practitioner;
import com.fhs.platform.service.user.integration.dto.IamUserDto;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Component
public class IamServiceIntegration {

    @Value("${iam.service.base.url:" + IamIntegrationConstants.DEFAULT_BASE_URL + "}")
    private String iamServiceBaseUrl;
    @Value("${iam.service.user.create.url}")
    private String iamServiceUserCreateUrl;
    @Autowired
    private RestTemplate restTemplate;

    @PostConstruct
    private void configBaseUrl() {
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(iamServiceBaseUrl));
    }


    public IamUserDto createUserPatient(Patient patient) {
        IamUserDto iamUserDto = new IamUserDto();
        iamUserDto.setUserId(UUID.randomUUID());
        iamUserDto.setAuthority(IamIntegrationConstants.USER_AUTHORITY);
        iamUserDto.setRoleName(IamIntegrationConstants.PATIENT_ROLE_NAME);
        iamUserDto.setPassword(IamIntegrationConstants.DEFAULT_PASSWORD);
        iamUserDto.setUsername(patient.getEmail());
        ResponseEntity<IamUserDto> iamUserDtoResponseEntity = restTemplate
                .postForEntity(iamServiceUserCreateUrl, iamUserDto, IamUserDto.class);
        if (iamUserDtoResponseEntity.getStatusCode().equals(HttpStatus.CREATED)) {
            return iamUserDtoResponseEntity.getBody();
        } else {
            throw new BusinessException();
        }
    }

    public IamUserDto createUserPractitioner(Practitioner practitioner) {
        IamUserDto iamUserDto = new IamUserDto();
        iamUserDto.setUserId(UUID.randomUUID());
        iamUserDto.setAuthority(IamIntegrationConstants.USER_AUTHORITY);
        iamUserDto.setRoleName(IamIntegrationConstants.PRACTITIONER_ROLE_NAME);
        iamUserDto.setPassword(IamIntegrationConstants.DEFAULT_PASSWORD);
        iamUserDto.setUsername(practitioner.getEmail());

        ResponseEntity<IamUserDto> iamUserDtoResponseEntity = restTemplate
                .postForEntity(iamServiceUserCreateUrl, iamUserDto, IamUserDto.class);
        if (iamUserDtoResponseEntity.getStatusCode().equals(HttpStatus.CREATED)) {
            return iamUserDtoResponseEntity.getBody();
        } else {
            throw new BusinessException();
        }
    }

}
