package com.fhs.platform.service.user.controller.request.create;

import com.fhs.platform.service.user.constants.gender.Gender;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ContactCreateDto {

    @NotNull
    private String name;
    @NotNull
    private Gender gender;
    @NotNull
    private String email;
    @NotNull
    private String phoneNumber;
    @NotNull
    private String organization;
    @NotNull
    private String relationship;
    @Valid
    private AddressCreateDto address;

}
