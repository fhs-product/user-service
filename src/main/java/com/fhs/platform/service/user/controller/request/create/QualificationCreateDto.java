package com.fhs.platform.service.user.controller.request.create;

import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fhs.platform.common.config.jackson.ZonedDateTimeDeserializer;
import com.fhs.platform.common.config.jackson.ZonedDateTimeSerializer;
import com.fhs.platform.common.constants.DateTimeFormatConstants;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.ZonedDateTime;

@Data
public class QualificationCreateDto {

    @NotNull
    private String code;
    @NotNull
    private String name;
    @NotNull
    @JsonDeserialize(using = ZonedDateTimeDeserializer.class)
    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormatConstants.ZONE_DATE_TIME_FORMAT)
    private ZonedDateTime  startValid;
    @NotNull
    @JsonDeserialize(using = ZonedDateTimeDeserializer.class)
    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormatConstants.ZONE_DATE_TIME_FORMAT)
    private ZonedDateTime  endValid;
    @NotNull
    private String issuer;

}
