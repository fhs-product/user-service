package com.fhs.platform.service.user.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fhs.platform.common.entity.BaseEntity;
import com.fhs.platform.service.user.constants.gender.Gender;
import com.fhs.platform.service.user.constants.maritalstatus.MaritalStatus;
import com.fhs.platform.service.user.constants.role.Role;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
public class Patient extends BaseEntity {

    private String avatarUrl;
    @NotNull
    private Gender gender;
    @NotNull
    private String familyName;
    @NotNull
    private String givenName;
    @NotNull
    private String phoneNumber;
    @NotNull
    private String email;
    @NotNull
    private Boolean active = true;
    private String prefixName;
    private String suffixName;

    private LocalDate birthDate;
    private Boolean deceased = false;
    private ZonedDateTime deceasedDateTime;
    private MaritalStatus maritalStatus;
    private String managingOrganization;
    private Role role = Role.Patient;
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "patient_practitioner",
            joinColumns = @JoinColumn(name = "patient_id"),
            inverseJoinColumns = @JoinColumn(name = "practitioner_id")
    )
    private Collection<Practitioner> practitioners;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<Identifier> identifiers;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<Address> addresses;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<Contact> contacts;


}
