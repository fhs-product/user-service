package com.fhs.platform.service.user.service.impl;


import com.fhs.platform.common.dto.DeleteResponse;
import com.fhs.platform.common.dto.PagingResponseDTO;
import com.fhs.platform.common.errors.exceptions.EmailAlreadyInUse;
import com.fhs.platform.common.errors.exceptions.IdentifierAlreadyInUse;
import com.fhs.platform.common.errors.exceptions.ResourceNotFoundException;
import com.fhs.platform.service.user.constants.UserConstants;
import com.fhs.platform.service.user.controller.PractitionerController;
import com.fhs.platform.service.user.controller.request.create.AddressCreateDto;
import com.fhs.platform.service.user.controller.request.create.IdentifierCreateDto;
import com.fhs.platform.service.user.controller.request.create.PractitionerCreateDto;
import com.fhs.platform.service.user.controller.request.create.QualificationCreateDto;
import com.fhs.platform.service.user.controller.request.update.AddressUpdateDto;
import com.fhs.platform.service.user.controller.request.update.IdentifierUpdateDto;
import com.fhs.platform.service.user.controller.request.update.PractitionerUpdateDto;
import com.fhs.platform.service.user.controller.request.update.QualificationUpdateDto;
import com.fhs.platform.service.user.controller.response.AddressResponseDto;
import com.fhs.platform.service.user.controller.response.AttachmentResponseDto;
import com.fhs.platform.service.user.controller.response.IdentifierResponseDto;
import com.fhs.platform.service.user.controller.response.PractitionerResponseDto;
import com.fhs.platform.service.user.controller.response.QualificationResponseDto;
import com.fhs.platform.service.user.domain.PractitionerFilter;
import com.fhs.platform.service.user.entity.Address;
import com.fhs.platform.service.user.entity.Identifier;
import com.fhs.platform.service.user.entity.Practitioner;
import com.fhs.platform.service.user.entity.Qualification;
import com.fhs.platform.service.user.integration.IamServiceIntegration;
import com.fhs.platform.service.user.integration.dto.IamUserDto;
import com.fhs.platform.service.user.repository.AddressRepository;
import com.fhs.platform.service.user.repository.IdentifierRepository;
import com.fhs.platform.service.user.repository.PractitionerRepository;
import com.fhs.platform.service.user.repository.QualificationRepository;
import com.fhs.platform.service.user.service.PractitionerService;
import com.fhs.platform.service.user.service.StorageService;
import com.fhs.platform.service.user.util.mapper.AddressMapper;
import com.fhs.platform.service.user.util.mapper.IdentifierMapper;
import com.fhs.platform.service.user.util.mapper.PractitionerMapper;
import com.fhs.platform.service.user.util.mapper.QualificationMapper;
import com.fhs.platform.service.user.util.search.PractitionerSpecification;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;


@Service
@AllArgsConstructor
public class PractitionerServiceImpl implements PractitionerService {

    private final PractitionerRepository practitionerRepository;
    private final IdentifierRepository identifierRepository;
    private final QualificationRepository qualificationRepository;
    private final AddressRepository addressRepository;

    private final PractitionerMapper practitionerMapper;
    private final QualificationMapper qualificationMapper;
    private final AddressMapper addressMapper;
    private final IdentifierMapper identifierMapper;
    private final StorageService storageService;
    private final IamServiceIntegration iamServiceIntegration;

    @Override
    public PagingResponseDTO<?> getPractitioners(Pageable pageable, PractitionerFilter filter) {
        Page<Practitioner> practitionerPage = practitionerRepository.findAll(new PractitionerSpecification(filter), pageable);
        return new PagingResponseDTO<>(practitionerPage.map(practitionerMapper::practitionerToPractitionerResponseDto));
    }

    @Override
    public QualificationResponseDto getPractitionerQualificationById(UUID practitionerId, UUID qualificationId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));

        return (practitioner.getQualifications().stream()
                .filter(qualification -> (!qualification.getIsDeleted()) && qualification.getId().equals(qualificationId))
                .map(qualificationMapper::qualificationToQualificationResponseDto)
                .findFirst().orElseThrow(() -> new ResourceNotFoundException(UserConstants.QUALIFICATION_NOT_FOUND_MESSAGE_EXCEPTION)));
    }

    @Override
    public QualificationResponseDto createPractitionerQualification(UUID practitionerId, QualificationCreateDto qualificationCreateDto) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));

        Qualification qualification = qualificationMapper.qualificationDtoToQualification(qualificationCreateDto);

        qualification.setPractitioner(practitioner);

        practitioner.getQualifications().add(qualification);

        Qualification savedQualification = qualificationRepository.save(qualification);
        practitionerRepository.save(practitioner);
        return qualificationMapper.qualificationToQualificationResponseDto(savedQualification);
    }

    @Override
    public QualificationResponseDto updatePractitionerQualificationById(UUID practitionerId, UUID qualificationId,
            QualificationUpdateDto qualificationUpdateDto) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        Qualification practitionerQualification = practitioner.getQualifications().stream()
                .filter(qualification -> (!qualification.getIsDeleted()) && qualification.getId().equals(qualificationId)).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.QUALIFICATION_NOT_FOUND_MESSAGE_EXCEPTION));

        qualificationMapper.updateQualificationFromDto(qualificationUpdateDto, practitionerQualification);

        Qualification savedQualification = qualificationRepository.save(practitionerQualification);

        practitionerRepository.save(practitioner);
        return qualificationMapper.qualificationToQualificationResponseDto(savedQualification);

    }

    @Override
    public DeleteResponse deletePractitionerQualificationById(UUID practitionerId, UUID qualificationId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        Qualification practitionerQualification = practitioner.getQualifications().stream()
                .filter(qualification -> (!qualification.getIsDeleted()) && qualification.getId().equals(qualificationId)).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.QUALIFICATION_NOT_FOUND_MESSAGE_EXCEPTION));

        practitionerQualification.setIsDeleted(true);
        Qualification savedQualification = qualificationRepository.save(practitionerQualification);

        practitionerRepository.save(practitioner);
        return new DeleteResponse(Boolean.TRUE);
    }

    @Override
    public List<QualificationResponseDto> getPractitionerQualifications(UUID practitionerId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        return practitioner.getQualifications().stream().filter(qualification -> !qualification.getIsDeleted())
                .map(qualificationMapper::qualificationToQualificationResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public AddressResponseDto getPractitionerAddressById(UUID practitionerId, UUID addressId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));

        return (practitioner.getAddresses().stream()
                .filter(address -> (!address.getIsDeleted()) && address.getId().equals(addressId)).map(addressMapper::addressToAddressResponseDto)
                .findFirst().orElseThrow(() -> new ResourceNotFoundException(UserConstants.ADDRESS_NOT_FOUND_MESSAGE_EXCEPTION)));
    }

    @Override
    public AddressResponseDto createPractitionerAddress(UUID practitionerId, AddressCreateDto addressCreateDto) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));

        Address practitionerAddress = addressMapper.addressDtoToAddress(addressCreateDto);

        practitionerAddress.setPractitioner(practitioner);
        practitioner.getAddresses().add(practitionerAddress);

        Address saveAddress = addressRepository.save(practitionerAddress);
        practitionerRepository.save(practitioner);
        return addressMapper.addressToAddressResponseDto(saveAddress);
    }

    @Override
    public AddressResponseDto updatePractitionerAddressById(UUID practitionerId, UUID addressId, AddressUpdateDto addressUpdateDto) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        Address practitionerAddress = practitioner.getAddresses().stream()
                .filter(address -> (!address.getIsDeleted()) && address.getId().equals(addressId)).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.ADDRESS_NOT_FOUND_MESSAGE_EXCEPTION));

        addressMapper.updateAddressFromDto(addressUpdateDto, practitionerAddress);

        Address saveAddress = addressRepository.save(practitionerAddress);

        practitionerRepository.save(practitioner);
        return addressMapper.addressToAddressResponseDto(saveAddress);
    }

    @Override
    public DeleteResponse deletePractitionerAddressById(UUID practitionerId, UUID addressId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        Address practitionerAddress = practitioner.getAddresses().stream()
                .filter(address -> (!address.getIsDeleted()) && address.getId().equals(addressId)).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.ADDRESS_NOT_FOUND_MESSAGE_EXCEPTION));

        practitionerAddress.setIsDeleted(true);

        Address saveAddress = addressRepository.save(practitionerAddress);

        practitionerRepository.save(practitioner);
        return new DeleteResponse(Boolean.TRUE);


    }

    @Override
    public List<AddressResponseDto> getPractitionerAddress(UUID practitionerId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        return practitioner.getAddresses().stream().filter(address -> !address.getIsDeleted()).map(addressMapper::addressToAddressResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public IdentifierResponseDto getPractitionerIdentifierById(UUID practitionerId, UUID identifierId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));

        return (practitioner.getIdentifiers().stream()
                .filter(identifier -> (!identifier.getIsDeleted()) && identifier.getId().equals(identifierId))
                .map(identifierMapper::identifierToIdentifierResponseDto)
                .findFirst().orElseThrow(() -> new ResourceNotFoundException(UserConstants.IDENTIFIER_NOT_FOUND_MESSAGE_EXCEPTION)));
    }

    @Override
    public IdentifierResponseDto createPractitionerIdentifier(UUID practitionerId, IdentifierCreateDto identifierCreateDto) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));

        Identifier practitionerIdentifier = identifierMapper.identifierDtoToIdentifier(identifierCreateDto);

        practitionerIdentifier.setPractitioner(practitioner);
        practitioner.getIdentifiers().add(practitionerIdentifier);

        Identifier savedIdentifier = identifierRepository.save(practitionerIdentifier);

        practitionerRepository.save(practitioner);
        return identifierMapper.identifierToIdentifierResponseDto(savedIdentifier);
    }

    @Override
    public IdentifierResponseDto updatePractitionerIdentifierById(UUID practitionerId, UUID identifierId, IdentifierUpdateDto identifierUpdateDto) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        Identifier practitionerIdentifier = practitioner.getIdentifiers().stream()
                .filter(identifier -> (!identifier.getIsDeleted()) && identifier.getId().equals(identifierId)).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.IDENTIFIER_NOT_FOUND_MESSAGE_EXCEPTION));

        identifierMapper.updateIdentifierFromDto(identifierUpdateDto, practitionerIdentifier);

        Identifier savedIdentifier = identifierRepository.save(practitionerIdentifier);

        practitionerRepository.save(practitioner);
        return identifierMapper.identifierToIdentifierResponseDto(savedIdentifier);
    }

    @Override
    public DeleteResponse deletePractitionerIdentifierById(UUID practitionerId, UUID identifierId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        Identifier practitionerIdentifier = practitioner.getIdentifiers().stream()
                .filter(identifier -> (!identifier.getIsDeleted()) && identifier.getId().equals(identifierId)).findFirst()
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.IDENTIFIER_NOT_FOUND_MESSAGE_EXCEPTION));

        practitionerIdentifier.setIsDeleted(true);

        Identifier savedIdentifier = identifierRepository.save(practitionerIdentifier);

        practitionerRepository.save(practitioner);
        return new DeleteResponse(Boolean.TRUE);
    }

    @Override
    public List<IdentifierResponseDto> getPractitionerIdentifiers(UUID practitionerId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        return practitioner.getIdentifiers().stream().filter(identifier -> !identifier.getIsDeleted())
                .map(identifierMapper::identifierToIdentifierResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public AttachmentResponseDto uploadAvatarToPractitioner(MultipartFile avatar, UUID practitionerId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        storageService.store(avatar, practitionerId);
        practitioner.setAvatarUrl(MvcUriComponentsBuilder
                .fromMethodName(PractitionerController.class, "getAvatar", avatar.getOriginalFilename(), practitionerId.toString()).build()
                .toString());
        practitionerRepository.save(practitioner);
        return new AttachmentResponseDto(practitioner.getAvatarUrl());
    }

    @Override
    public Resource loadAvatarFile(String filename, UUID practitionerId) {
        return storageService.loadAsResource(filename, practitionerId);
    }

    @Override
    public PractitionerResponseDto getPractitionerById(UUID practitionerId) {
        return practitionerMapper.practitionerToPractitionerResponseDto(practitionerRepository.findById(practitionerId)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION)));
    }

    @Override
    public PractitionerResponseDto createPractitioner(PractitionerCreateDto practitionerDto) {
        Practitioner practitioner = practitionerMapper.practitionerDtoToPractitioner(practitionerDto);
        if (practitionerRepository.findByEmailAndIsDeleted(practitioner.getEmail(), false).isPresent()) {
            throw new EmailAlreadyInUse(practitioner.getEmail());
        }
        practitioner.getIdentifiers().forEach(identifier -> {
            if (identifierRepository.findByIdentifierTypeAndValue(identifier.getIdentifierType(), identifier.getValue()).isPresent()) {
                throw new IdentifierAlreadyInUse(identifier.getIdentifierType(), identifier.getValue());
            }
            identifier.setPractitioner(practitioner);
        });
        IamUserDto iamUserDto = iamServiceIntegration.createUserPractitioner(practitioner);
        practitioner.setId(iamUserDto.getUserId());

        practitioner.getAddresses().forEach(address -> address.setPractitioner(practitioner));
        practitioner.getQualifications().forEach(qualification -> qualification.setPractitioner(practitioner));

        return practitionerMapper.practitionerToPractitionerResponseDto(practitionerRepository.save(practitioner));
    }

    @Override
    public PractitionerResponseDto updatePractitionerById(UUID practitionerId, PractitionerUpdateDto practitionerUpdateDto) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        practitionerMapper.updatePatientFromDto(practitionerUpdateDto, practitioner);

        return practitionerMapper.practitionerToPractitionerResponseDto(practitionerRepository.save(practitioner));
    }

    @Override
    public DeleteResponse deletePractitionerById(UUID practitionerId) {
        Practitioner practitioner = practitionerRepository.findByIdAndIsDeleted(practitionerId, false)
                .orElseThrow(() -> new ResourceNotFoundException(UserConstants.PRACTITIONER_NOT_FOUND_MESSAGE_EXCEPTION));
        practitioner.setIsDeleted(true);

        return new DeleteResponse(Boolean.TRUE);

    }

}
