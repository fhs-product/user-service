package com.fhs.platform.service.user.controller.request.update;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fhs.platform.common.config.jackson.LocalDateDeserializer;
import com.fhs.platform.common.config.jackson.LocalDateSerializer;
import com.fhs.platform.common.constants.DateTimeFormatConstants;
import java.time.LocalDate;
import java.util.Set;
import lombok.Data;

@Data
public class PatientUpdateDto {

    private String phoneNumber;
    private String familyName;
    private String givenName;
    private String prefixName;
    private String suffixName;
    private String email;
    private String gender;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormatConstants.DATE_FORMAT)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)

    private LocalDate birthDate;
    private String maritalStatus;
    private String managingOrganization;
    private Set<IdentifierUpdateDto> identifiers;
    private Set<AddressUpdateDto> addresses;
    private Set<ContactUpdateDto> contacts;

}
