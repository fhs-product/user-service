package com.fhs.platform.service.user.controller.response;

import java.time.ZonedDateTime;
import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fhs.platform.common.config.jackson.ZonedDateTimeDeserializer;
import com.fhs.platform.common.config.jackson.ZonedDateTimeSerializer;
import com.fhs.platform.common.constants.DateTimeFormatConstants;
import lombok.Data;

@Data
public class QualificationResponseDto {

    private UUID id;
    private String code;
    private String name;
    @JsonDeserialize(using = ZonedDateTimeDeserializer.class)
    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormatConstants.ZONE_DATE_TIME_FORMAT)
    private ZonedDateTime  startValid;
    @JsonDeserialize(using = ZonedDateTimeDeserializer.class)
    @JsonSerialize(using = ZonedDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeFormatConstants.ZONE_DATE_TIME_FORMAT)
    private ZonedDateTime endValid;
    private String issuer;

}
