package com.fhs.platform.service.user.constants.role;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Role {
    Patient("Patient", "Patient"),
    Practitioner("Practitioner", "Practitioner");
    private final String code;
    private final String description;
}
