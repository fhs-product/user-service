package com.fhs.platform.service.user.constants.identifiertype;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum IdentifierType {
    Passport("PPN", "Passport Number");
    private final String code;
    private final String description;
}
