package com.fhs.platform.service.user.service;

import java.nio.file.Path;
import java.util.UUID;
import java.util.stream.Stream;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {


    Path store(MultipartFile file, UUID entityId);
    Stream<Path> loadAll(UUID entityId);
    Resource loadAsResource(String filename,UUID entityId);
    void deleteAll(UUID entityId);

}
