package com.fhs.platform.service.user.constants.responsestatus;

public enum ResponseStatus {
    SUCCESS,
    FAILURE
}
