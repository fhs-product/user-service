package com.fhs.platform.service.user.constants.patientcontactrelationship;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PatientContactRelationship {
    EmergencyContact('C', "Emergency Contact"),
    Employer('E', "Employer"),
    NextOfKin('N', "Next-of-Kin"),
    Unknown('U', "Unknown");
    private final Character code;
    private final String description;
}
