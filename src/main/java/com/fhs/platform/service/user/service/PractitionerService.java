package com.fhs.platform.service.user.service;

import com.fhs.platform.common.dto.DeleteResponse;
import com.fhs.platform.common.dto.PagingResponseDTO;
import com.fhs.platform.service.user.controller.request.create.AddressCreateDto;
import com.fhs.platform.service.user.controller.request.create.IdentifierCreateDto;
import com.fhs.platform.service.user.controller.request.create.PractitionerCreateDto;
import com.fhs.platform.service.user.controller.request.create.QualificationCreateDto;
import com.fhs.platform.service.user.controller.request.update.AddressUpdateDto;
import com.fhs.platform.service.user.controller.request.update.IdentifierUpdateDto;
import com.fhs.platform.service.user.controller.request.update.PractitionerUpdateDto;
import com.fhs.platform.service.user.controller.request.update.QualificationUpdateDto;
import com.fhs.platform.service.user.controller.response.*;
import com.fhs.platform.service.user.domain.PractitionerFilter;
import java.util.List;
import java.util.UUID;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface PractitionerService {

    PractitionerResponseDto getPractitionerById(UUID practitionerId);

    PractitionerResponseDto createPractitioner(PractitionerCreateDto practitionerDto);

    PractitionerResponseDto updatePractitionerById(UUID practitionerId, PractitionerUpdateDto practitionerUpdateDto);

    DeleteResponse deletePractitionerById(UUID practitionerId);

    PagingResponseDTO<?> getPractitioners(Pageable pageable, PractitionerFilter filter);

    QualificationResponseDto getPractitionerQualificationById(UUID practitionerId, UUID qualificationId);

    QualificationResponseDto createPractitionerQualification(UUID practitionerId, QualificationCreateDto qualificationDto);

    QualificationResponseDto updatePractitionerQualificationById(UUID practitionerId, UUID qualificationId,
            QualificationUpdateDto qualificationUpdateDto);

    DeleteResponse deletePractitionerQualificationById(UUID practitionerId, UUID qualificationId);

    List<QualificationResponseDto> getPractitionerQualifications(UUID practitionerId);

    AddressResponseDto getPractitionerAddressById(UUID practitionerId, UUID addressId);

    AddressResponseDto createPractitionerAddress(UUID practitionerId, AddressCreateDto addressDto);

    AddressResponseDto updatePractitionerAddressById(UUID practitionerId, UUID addressId, AddressUpdateDto addressDto);

    DeleteResponse deletePractitionerAddressById(UUID practitionerId, UUID addressId);

    List<AddressResponseDto> getPractitionerAddress(UUID practitionerId);

    IdentifierResponseDto getPractitionerIdentifierById(UUID practitionerId, UUID qualificationId);

    IdentifierResponseDto createPractitionerIdentifier(UUID practitionerId, IdentifierCreateDto identifierDto);

    IdentifierResponseDto updatePractitionerIdentifierById(UUID practitionerId, UUID qualificationId, IdentifierUpdateDto identifierDto);

    DeleteResponse deletePractitionerIdentifierById(UUID practitionerId, UUID qualificationId);

    List<IdentifierResponseDto> getPractitionerIdentifiers(UUID practitionerId);

    AttachmentResponseDto uploadAvatarToPractitioner(MultipartFile avatar, UUID patientId);

    Resource loadAvatarFile(String filename, UUID practitionerId);

}
