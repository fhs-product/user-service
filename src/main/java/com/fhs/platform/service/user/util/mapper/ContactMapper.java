package com.fhs.platform.service.user.util.mapper;

import com.fhs.platform.service.user.controller.request.create.ContactCreateDto;
import com.fhs.platform.service.user.controller.request.update.ContactUpdateDto;
import com.fhs.platform.service.user.controller.response.ContactResponseDto;
import com.fhs.platform.service.user.entity.Contact;
import org.mapstruct.BeanMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", uses = {
}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ContactMapper {

    @Mapping(target = "address", ignore = true)
    @Mapping(target = "patient", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Contact contactDtoToContact(ContactCreateDto contactCreateDto);


    ContactResponseDto contactToContactResponseDto(Contact contact);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(target = "address", ignore = true)
    void updateContactFromDto(ContactUpdateDto contactUpdateDto, @MappingTarget Contact contact);

}
