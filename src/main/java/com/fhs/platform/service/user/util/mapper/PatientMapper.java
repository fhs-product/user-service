package com.fhs.platform.service.user.util.mapper;

import com.fhs.platform.service.user.controller.request.create.PatientCreateDto;
import com.fhs.platform.service.user.controller.request.update.PatientUpdateDto;
import com.fhs.platform.service.user.controller.response.PatientResponseDto;
import com.fhs.platform.service.user.entity.Patient;
import org.mapstruct.BeanMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;


@Mapper(componentModel = "spring", uses = {
}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PatientMapper {

    Patient patientDtoToPatient(PatientCreateDto patientCreateDto);

    @Mapping(target = "birthDate", dateFormat = "yyyy-MM-dd")
    PatientResponseDto patientToPatientResponseDto(Patient patient);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updatePatientFromDto(PatientUpdateDto patientUpdateDto, @MappingTarget Patient patient);

}
