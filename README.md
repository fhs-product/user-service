# README #

### Service Configuration Parametter ###

* DATASOURCE_URL:        jdbc:postgres://{database_url}:{database_port}/{database_name}
* DATASOURCE_PASSWORD:   {database_password}
* DATASOURCE_USERNAME:   {database_user}
* IAM_SERVICE:           {iam_service_url}

-port: 8083
### Deploy to Heroku ###
* Go to root folder `cd appointment-service`
* Specific Java version (11) by creating a `system.properties` file : `echo 'java.runtime.version=11' > system.properties`
* Create heroku project `heroku create -a akahealth-user-service`
* Addon PostgreSQL DB : `heroku addons`
* Check DB config: `heroku config`
* Commit, Push code and deploy to heroku: `gaa` && `gcmsg 'rebase code to heroku'` && `git push heroku master` 

